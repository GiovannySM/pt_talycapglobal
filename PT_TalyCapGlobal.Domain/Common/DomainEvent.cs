using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PT_TalyCapGlobal.Domain.Common
{
    public interface IHasDomainEvent
    {
        [NotMapped]
        public List<DomainEvent> DomainEvents { get; set; }
    }

    public abstract class DomainEvent
    {
        protected DomainEvent()
        {
            DateOccurred = DateTimeOffset.UtcNow;
        }

        [NotMapped]
        public DateTimeOffset DateOccurred { get; protected set; } = DateTime.UtcNow;
    }
}
