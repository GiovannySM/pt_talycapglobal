using PT_TalyCapGlobal.Domain.Common;

namespace PT_TalyCapGlobal.Domain.Events
{
    public class EntitiesCompletedEvent<T> : DomainEvent
    {
        public EntitiesCompletedEvent(T Entry)
        {
            Entity = Entry;
        }

        public T Entity { get; }
    }
}
