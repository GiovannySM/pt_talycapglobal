using PT_TalyCapGlobal.Domain.Common;

namespace PT_TalyCapGlobal.Domain.Events
{
    public class EntitiesCreatedEvent<T> : DomainEvent
    {
        public EntitiesCreatedEvent(T item)
        {
            Item = item;
        }

        public T Item { get; }
    }
}
