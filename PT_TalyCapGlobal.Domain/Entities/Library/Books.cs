using PT_TalyCapGlobal.Domain.Common;
using PT_TalyCapGlobal.Domain.Events;
using System.Collections.Generic;

namespace PT_TalyCapGlobal.Domain.Entities.Library
{
    public class Books : AuditableEntity, IHasDomainEvent
    {
        public int ISBN { get; set; }
        public int EditorialsId { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public string N_Pages { get; set; }

        private bool _done;
        public bool Done
        {
            get => _done;
            set
            {
                if (value == true && _done == false)
                {
                    DomainEvents.Add(new EntitiesCompletedEvent<Books>(this));
                }
                _done = value;
            }
        }
        public List<DomainEvent> DomainEvents { get; set; } = new List<DomainEvent>();

    }
}
