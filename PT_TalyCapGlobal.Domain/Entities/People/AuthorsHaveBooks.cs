using PT_TalyCapGlobal.Domain.Common;
using PT_TalyCapGlobal.Domain.Entities.Library;
using PT_TalyCapGlobal.Domain.Events;
using System.Collections.Generic;

namespace PT_TalyCapGlobal.Domain.Entities.People
{
    public class AuthorsHaveBooks : AuditableEntity, IHasDomainEvent
    {
        public int AuthorsId { get; set; }
        public virtual Authors Authors { get; set; }
        public int BooksISBN { get; set; }
        public virtual Books Books { get; set; }

        private bool _done;
        public bool Done
        {
            get => _done;
            set
            {
                if (value == true && _done == false)
                {
                    DomainEvents.Add(new EntitiesCompletedEvent<AuthorsHaveBooks>(this));
                }
                _done = value;
            }
        }
        public List<DomainEvent> DomainEvents { get; set; } = new List<DomainEvent>();
    }
}
