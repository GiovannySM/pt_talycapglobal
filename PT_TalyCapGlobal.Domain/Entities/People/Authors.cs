using PT_TalyCapGlobal.Domain.Common;
using PT_TalyCapGlobal.Domain.Events;
using System.Collections.Generic;

namespace PT_TalyCapGlobal.Domain.Entities.People
{
    public class Authors : AuditableEntity, IHasDomainEvent
    {
        public int AuthorsId { get; set; }
        public string Names { get; set; }
        public string Surnames { get; set; }

        private bool _done;
        public bool Done
        {
            get => _done;
            set
            {
                if (value == true && _done == false)
                {
                    DomainEvents.Add(new EntitiesCompletedEvent<Authors>(this));
                }
                _done = value;
            }
        }
        public List<DomainEvent> DomainEvents { get; set; } = new List<DomainEvent>();

    }
}
