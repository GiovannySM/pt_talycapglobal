
using Microsoft.AspNetCore.Identity;

namespace PT_TalyCapGlobal.Domain.Identity
{
    public class ApplicationUser : IdentityUser
    { }
}
