import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/User';
import { ApplicationPaths, LoginActions } from './auth-authorize.const';
import { JwtTokenResponse } from '../models/JwtTokenResponse';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({ providedIn: 'root' })

export class AuthService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  JwtTokenResponse: JwtTokenResponse = new JwtTokenResponse();
  User: User = new User();
  invalidLogin: boolean;
  isLoggedin: boolean;

  constructor(
    private router: Router,
    private http: HttpClient,
    private JwtHelper: JwtHelperService
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(ApplicationPaths.User));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(formData) {    
    return this.http.post<any>(LoginActions.Login, formData, { headers: new HttpHeaders({ "Content-Type": "application/json" }) })
      .pipe(map(user => {
        const IsAuthenticated = (<any>user).success;
        if (IsAuthenticated) {
          const token = (<any>user).data.token;
          const JwtObject = JSON.parse(JSON.stringify(this.JwtHelper.decodeToken(token)));
          for (var i in JwtObject) {
            var cant = i.split("/").length - 1
            var property = i.split("/")[cant];
            switch (property) {
              case 'aud':
                this.JwtTokenResponse.aud = JwtObject[i];
                break;
              case 'emailaddress':
                this.JwtTokenResponse.emailaddress = JwtObject[i];
                this.User.firstName = JwtObject[i];
                this.User.lastName = JwtObject[i];
                this.User.username = JwtObject[i];
                break;
              case 'exp':
                this.JwtTokenResponse.exp = JwtObject[i];
                break;
              case 'iss':
                this.JwtTokenResponse.iss = JwtObject[i];
                break;
              case 'name':
                this.JwtTokenResponse.name = JwtObject[i];
                break;
              case 'nameidentifier':
                this.JwtTokenResponse.nameidentifier = JwtObject[i];
                this.User.id = JwtObject[i];
                break;
              case 'role':
                this.JwtTokenResponse.role = JwtObject[i];
                this.User.role = JwtObject[i];
                break;
              case 'windowsaccountname':
                this.JwtTokenResponse.windowsaccountname = JwtObject[i];
                break;
              default:
            }
          }
          this.User.token = token;
          localStorage.setItem(ApplicationPaths.TokenKey, token);
          localStorage.setItem(ApplicationPaths.TokenExpiredKey, this.JwtHelper.getTokenExpirationDate(token).toUTCString());
          localStorage.setItem(ApplicationPaths.UserKey, JSON.stringify(this.User));
          this.invalidLogin = false;
          this.isLoggedin = true;
          this.userSubject.next(this.User);
          return user;
        }
      }, err => {
        console.log(err);
        (error: HttpErrorResponse) => {
          if (error.status === 400) {
            const errors: Array<any> = error.error;
            errors.forEach(clientError => {
              console.log(clientError.message);
            });
          }
        }
        this.invalidLogin = true;
        this.isLoggedin = false;
      },
       
      ));
  }

  logout() {
    localStorage.removeItem(ApplicationPaths.TokenKey);
    localStorage.removeItem(ApplicationPaths.TokenExpiredKey);
    localStorage.removeItem(ApplicationPaths.UserKey);
    this.router.navigateByUrl(ApplicationPaths.Login);
    this.userSubject.next(null);
    this.isLoggedin = false;
  }
}
