"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationPaths = exports.TokenStorange = exports.UserStorange = exports.LoginActions = exports.LogoutActions = exports.BaseAddressBackEnd = exports.QueryParameterNames = exports.ReturnUrlType = exports.ApplicationName = void 0;
exports.ApplicationName = 'PT_TalyCapGlobal.Client';
exports.ReturnUrlType = 'returnUrl';
exports.QueryParameterNames = {
    ReturnUrl: exports.ReturnUrlType,
    Message: 'message'
};
exports.BaseAddressBackEnd = {
    Login: 'https://localhost:44352/',
    BaseUrl: 'https://localhost:44352/'
};
exports.LogoutActions = {
    Logout: "auth-logout",
};
exports.LoginActions = {
    Login: exports.BaseAddressBackEnd.Login + "Account/v1.0/LoginAsync",
};
exports.UserStorange = {
    UserKey: "User"
};
exports.TokenStorange = {
    TokenKey: "JwtToken",
    TokenExpiredKey: "JwtTokenExpired",
};
var applicationPaths = {
    DefaultLoginRedirectPath: '/',
    ApiAuthorizationClientConfigurationUrl: "_configuration/" + exports.ApplicationName,
    Login: "auth-login",
    LogOut: "" + exports.LogoutActions.Logout,
    LoginPathComponents: [],
    LoginFailedPathComponents: [],
    LoginCallbackPathComponents: [],
    RegisterPathComponents: [],
    ProfilePathComponents: [],
    LogOutPathComponents: [],
    LoggedOutPathComponents: [],
    LogOutCallbackPathComponents: [],
    TokenKey: exports.TokenStorange.TokenKey,
    TokenExpiredKey: exports.TokenStorange.TokenExpiredKey,
    Token: localStorage.getItem(exports.TokenStorange.TokenKey),
    UserKey: exports.UserStorange.UserKey,
    User: localStorage.getItem(exports.UserStorange.UserKey),
    BaseUrl: exports.BaseAddressBackEnd.BaseUrl
};
applicationPaths = __assign(__assign({}, applicationPaths), { LoginPathComponents: applicationPaths.Login.split('/') });
exports.ApplicationPaths = applicationPaths;
//# sourceMappingURL=auth-authorize.const.js.map