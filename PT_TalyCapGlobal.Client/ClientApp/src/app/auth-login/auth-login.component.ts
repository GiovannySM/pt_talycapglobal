import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationPaths } from './auth-authorize.const';
import { AuthService } from './auth-service';
import { first } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.css']
})
export class AuthLoginComponent implements OnInit {
  formModel = {
    UserName: '',
    Password: ''
  }
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.userValue) {     
      this.router.navigate([ApplicationPaths.DefaultLoginRedirectPath]);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    let returnUrl: any = '';
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.formModel.UserName = this.f.username.value;
      this.formModel.Password = this.f.password.value
    }
    this.loading = true;
    const credentials = JSON.stringify(this.formModel);
    this.authenticationService.login(credentials)
      .pipe(first())
      .subscribe({
        next: () => {
          returnUrl = this.route.snapshot.queryParams['returnUrl'] || ApplicationPaths.DefaultLoginRedirectPath;
          this.router.navigateByUrl(returnUrl);          
        },
        error: error => {        
          const errors: Array<any> = error.error.errors;
          errors.forEach(clientError => {
            this.error = clientError.message;
          });          
          this.loading = false;
        }
      });
    
    this.loading = false;
    //this.router.navigate([ApplicationPaths.DefaultLoginRedirectPath]);
  }
  
}
