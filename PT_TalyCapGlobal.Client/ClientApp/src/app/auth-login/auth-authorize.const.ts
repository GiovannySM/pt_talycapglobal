export const ApplicationName = 'PT_TalyCapGlobal.Client';

export const ReturnUrlType = 'returnUrl';

export const QueryParameterNames = {
  ReturnUrl: ReturnUrlType,
  Message: 'message'
};

export const BaseAddressBackEnd = {
  Login: 'https://localhost:44352/',
  BaseUrl: 'https://localhost:44352/'
};

export const LogoutActions = {
  Logout: "auth-logout",
};

export const LoginActions = {
  Login: `${BaseAddressBackEnd.Login}Account/v1.0/LoginAsync`,
};

export const UserStorange = {
  UserKey:"User"
}

export const TokenStorange = {
  TokenKey: "JwtToken",
  TokenExpiredKey: "JwtTokenExpired",
}

let applicationPaths: ApplicationPathsType = {
  DefaultLoginRedirectPath: '/',
  ApiAuthorizationClientConfigurationUrl: `_configuration/${ApplicationName}`,
  Login: "auth-login",
  LogOut: `${LogoutActions.Logout}`,
  LoginPathComponents: [],
  LoginFailedPathComponents: [],
  LoginCallbackPathComponents: [],
  RegisterPathComponents: [],
  ProfilePathComponents: [],
  LogOutPathComponents: [],
  LoggedOutPathComponents: [],
  LogOutCallbackPathComponents: [],
  TokenKey: TokenStorange.TokenKey,
  TokenExpiredKey: TokenStorange.TokenExpiredKey,
  Token: localStorage.getItem(TokenStorange.TokenKey),
  UserKey: UserStorange.UserKey,
  User: localStorage.getItem(UserStorange.UserKey),
  BaseUrl: BaseAddressBackEnd.BaseUrl
};

applicationPaths = {
  ...applicationPaths,
  LoginPathComponents: applicationPaths.Login.split('/'),
};

interface ApplicationPathsType {
  readonly DefaultLoginRedirectPath: string;
  readonly ApiAuthorizationClientConfigurationUrl: string;
  readonly Login: string;
  readonly LogOut: string;
  readonly LoginPathComponents: string[];
  readonly LoginFailedPathComponents: string[];
  readonly LoginCallbackPathComponents: string[];
  readonly RegisterPathComponents: string[];
  readonly ProfilePathComponents: string[];
  readonly LogOutPathComponents: string[];
  readonly LoggedOutPathComponents: string[];
  readonly LogOutCallbackPathComponents: string[];
  readonly Token: string;
  readonly TokenKey: string;
  readonly TokenExpiredKey: string;
  readonly UserKey: string;
  readonly User: string;
  readonly BaseUrl: string;
}

export const ApplicationPaths: ApplicationPathsType = applicationPaths;
