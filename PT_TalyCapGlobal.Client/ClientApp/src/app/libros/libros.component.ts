import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { isUndefined } from 'util';
import { ApplicationPaths } from '../auth-login/auth-authorize.const';
import { FiltersBooks } from '../models/FiltersBooks';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit, OnDestroy {
  public books: BooksDTO[];

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  submitted: boolean;
  fileName = 'ExcelSheet.xlsx';

  constructor(public http: HttpClient) {
    this.getDataInit();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  model = new FiltersBooks();

  onSubmit() {
    this.submitted = true;
    this.getData();
  }

  getData() {

    var QueryString = ''

    if (isUndefined(this.model.AuthorName) && isUndefined(this.model.StartDate) && isUndefined(this.model.FinalhDate)) {
      this.getDataInit();
    }

    if (!isUndefined(this.model.AuthorName)) {
      QueryString = `${QueryString}AuthorName=${this.model.AuthorName}`
    }

    if (!isUndefined(this.model.StartDate) && isUndefined(this.model.FinalhDate)) {
      if (QueryString === '') {
        QueryString = `StartDate=${this.model.StartDate}`
      }
      else {
        QueryString = `&StartDate=${this.model.StartDate}`
      }
    }

    if (!isUndefined(this.model.FinalhDate)) {
      if (QueryString === '') {
        QueryString = `FinalhDate=${this.model.FinalhDate}`
      }
      else {
        QueryString = `&FinalhDate=${this.model.FinalhDate}`
      }
    }

    this.http.get<Root>(`${ApplicationPaths.BaseUrl}Library/Books/v1.0/getBookAsync?${QueryString}`)
      .subscribe(result => {
        this.books = result.data;
      }, error => console.error(error));
  }

  getDataInit() {
    this.http.get<Root>(`${ApplicationPaths.BaseUrl}Library/Books/v1.0/getBookAsync`)
      .subscribe(result => {
        this.books = result.data;
      }, error => console.error(error));
  }

  clearFilters() {
    this.model = new FiltersBooks();
    this.getDataInit();
  }

  exportExcel() {
    /* pass here the table id */
    let element = document.getElementById('excel-books');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }
}

interface Root {
  data: BooksDTO[];
  message: string;
  success: boolean;
}

interface BooksDTO {
  ISBN: number;
  Title: string;
  Synopsis: string;
  N_Pages: string;
  AutorName: string;
  PublishDate: string
}
