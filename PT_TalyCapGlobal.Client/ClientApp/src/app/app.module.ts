import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { JwtInterceptor, JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { AppRoutingModule } from './app-routing.module';
import { ApplicationPaths } from './auth-login/auth-authorize.const';
import { LibrosComponent } from './libros/libros.component';
import { AuthAInterceptor } from './auth-login/auth-interceptor';
import { DataTablesModule } from 'angular-datatables';

export function tokenGetter() {
  return ApplicationPaths.Token;
}

const JWT_Module_Options: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter
  }
};

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    JwtModule.forRoot(JWT_Module_Options),
    FormsModule,
    DataTablesModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    AuthLoginComponent,
    LibrosComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthAInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
