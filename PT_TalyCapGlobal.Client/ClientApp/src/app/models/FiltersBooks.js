"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FiltersBooks = void 0;
var FiltersBooks = /** @class */ (function () {
    function FiltersBooks(AuthorName, StartDate, FinalhDate) {
        this.AuthorName = AuthorName;
        this.StartDate = StartDate;
        this.FinalhDate = FinalhDate;
    }
    return FiltersBooks;
}());
exports.FiltersBooks = FiltersBooks;
//# sourceMappingURL=FiltersBooks.js.map