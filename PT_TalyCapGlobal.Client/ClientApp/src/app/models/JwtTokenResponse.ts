export class JwtTokenResponse {
  name: string;
  nameidentifier: string;
  emailaddress: string;
  windowsaccountname: string;
  role: string;
  exp: number;
  iss: string;
  aud: string;
}
