import { Component } from '@angular/core';
import { AuthService } from './auth-login/auth-service';
import { User } from './models/User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  user: User;

  constructor(private authenticationService: AuthService) {
    this.authenticationService.user.subscribe(x => this.user = x);
  }

  get isAdmin() {
    return this.user && this.user.role === "Administrador";
  }

  logout() {
    this.authenticationService.logout();
  }
}
