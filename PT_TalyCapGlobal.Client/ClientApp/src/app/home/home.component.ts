import { Component } from '@angular/core';
import { AuthService } from '../auth-login/auth-service';
import { User } from '../models/User';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  loading = false;
  user: User;
  userFromApi: User;

  constructor(
    private authenticationService: AuthService
  ) {
    this.user = this.authenticationService.userValue;
  }

  ngOnInit() {
    this.loading = true;
  }
}
