using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PT_TalyCapGlobal.Common.Helpers;
using PT_TalyCapGlobal.DataAccess.Context;
using PT_TalyCapGlobal.Domain.Identity;
using PT_TalyCapGlobal.Services.ApiValidators.Filters;
using PT_TalyCapGlobal.Services.Events;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.Identity;
using PT_TalyCapGlobal.Services.IEvents;
using PT_TalyCapGlobal.Services.IIdentity;
using PT_TalyCapGlobal.Services.IServices;
using PT_TalyCapGlobal.Services.Services;
using System;

namespace PT_TalyCapGlobal.Infrastructure.Dependency
{
    public static class InjectionServices
    {
        public static IServiceCollection InjectInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = HelperConfiguration.GetConnectionConfiguration("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString,
                                        b => b
                                        .MigrationsHistoryTable("TalyCapGlobalHistoryEFCore", "Param")
                                        .MigrationsAssembly("PT_TalyCapGlobal.DataAccessMigrations")
                                    ));

            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                         .AddRoles<IdentityRole>()
                         .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            services.AddIdentityServer()
              .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthentication()
                .AddIdentityServerJwt();


            services.AddTransient<IDateTimeService, DateTimeService>();
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IDomainEventService, DomainEventService>();

            //Add Filters
            services.AddMvc(option =>
            {
                option.Filters.Add<ValidationFilters>();
            }).AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
            });

            return services;
        }
    }
}
