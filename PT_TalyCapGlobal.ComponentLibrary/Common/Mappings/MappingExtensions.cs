using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.ComponentLibrary.Common.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Common.Mappings
{
    public static class MappingExtensions
    {
        public static Task<PaginatedList<TDestination>> PaginatedListAsync<TDestination>(this IQueryable<TDestination> queryable, int pageNumber, int pageSize)
            => PaginatedList<TDestination>.CreateAsync(queryable, pageNumber, pageSize);

        public static Task<List<TDestination>> ProjectToListAsync<TDestination>(this IQueryable queryable, IConfigurationProvider configuration)
            => queryable.ProjectTo<TDestination>(configuration).ToListAsync();

        public static TDestination Map<TSource, TDestination>(TSource source)
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TSource, TDestination>();
            });
            return (TDestination)config.CreateMapper().Map<TDestination>(source);
        }
    }

}
