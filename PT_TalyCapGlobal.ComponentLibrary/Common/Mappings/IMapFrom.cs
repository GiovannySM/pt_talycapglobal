using AutoMapper;

namespace PT_TalyCapGlobal.ComponentLibrary.Common.Mappings
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
