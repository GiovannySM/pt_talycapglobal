using MediatR;
using Microsoft.Extensions.Logging;
using PT_TalyCapGlobal.Domain.Events;
using PT_TalyCapGlobal.Services.Events;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.EventHandlers
{
    public class AuthorCreatedEventHandler : INotificationHandler<DomainEventNotification<EntitiesCreatedEvent<Domain.Entities.Library.Books>>>
    {
        private readonly ILogger<AuthorCreatedEventHandler> _logger;

        public AuthorCreatedEventHandler(ILogger<AuthorCreatedEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(DomainEventNotification<EntitiesCreatedEvent<Domain.Entities.Library.Books>> notification, CancellationToken cancellationToken)
        {
            var domainEvent = notification.DomainEvent;

            _logger.LogInformation("ComponentLibrary Domain Event: {DomainEvent}", domainEvent.GetType().Name);

            return Task.CompletedTask;
        }
    }
}