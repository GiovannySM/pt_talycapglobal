
using PT_TalyCapGlobal.Services.IWrappersCQRS;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands
{
    public class deleteBooksCommand : IRequestWrapper<bool>
    {
        public int bookId { get; }
        public deleteBooksCommand(int bookId)
        {
            this.bookId = bookId;
        }
    }
}
