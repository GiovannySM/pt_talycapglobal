using AutoMapper;
using PT_TalyCapGlobal.ComponentLibrary.Common.Mappings;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Library;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands
{
    public class createBooksCommandHandler : IHandlerWrapper<createBooksCommand, bool>
    {

        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public createBooksCommandHandler(IApplicationDbContext context, IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<bool>> Handle(createBooksCommand request, CancellationToken cancellationToken)
        {
            return await CreateBookAsync(request.BooksDTO);
        }

        private async Task<Response<bool>> CreateBookAsync(BooksDTO BooksDTO)
        {
            try
            {
                var Authors = await Context.Authors.FindAsync(int.Parse(BooksDTO.AutorName));

                var Book = MappingExtensions.Map<BooksDTO, Domain.Entities.Library.Books>(BooksDTO);
                var book = Context.Books.Add(Book).Entity;
                await Context.SaveChangesAsync();

                Domain.Entities.People.AuthorsHaveBooks authorsHaveBooks = new Domain.Entities.People.AuthorsHaveBooks
                {
                    AuthorsId = Authors.AuthorsId,
                    BooksISBN = book.ISBN
                };

                Context.AuthorsHaveBooks.Add(authorsHaveBooks);
                await Context.SaveChangesAsync();

                return await Task.FromResult(Response.Ok(true));
            }
            catch (Exception)
            {
                return await Task.FromResult(Response.Fail<bool>(false));
            }
        }
    }
}
