using AutoMapper;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands
{
    public class deleteBooksCommandHandler : IHandlerWrapper<deleteBooksCommand, bool>
    {
        private IApplicationDbContext context;
        private IMapper _mapper;

        public deleteBooksCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }
        public async Task<Response<bool>> Handle(deleteBooksCommand request, CancellationToken cancellationToken)
        {
            return await deleteAuthorAsync(request.bookId);
        }

        private async Task<Response<bool>> deleteAuthorAsync(int bookId)
        {
            try
            {
                var author = await context.Books.FindAsync(bookId);
                if (author == null)
                    throw new BusinessException($"El Libro con ISBN :{bookId}, no exite.");
                context.Books.Remove(author);
                await context.SaveChangesAsync();
                return await Task.FromResult(Response.Ok(true));
            }
            catch (Exception ex)
            {
                throw new BusinessException(ex.Message);
            }
        }
    }
}
