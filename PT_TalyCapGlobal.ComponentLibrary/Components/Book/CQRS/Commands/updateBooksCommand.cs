
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Library;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands
{
    public class updateBooksCommand : IRequestWrapper<bool>
    {
        public BooksDTO BooksDTO { get; }
        public updateBooksCommand(BooksDTO BooksDTO)
        {
            this.BooksDTO = BooksDTO;
        }
    }
}
