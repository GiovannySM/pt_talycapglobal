using AutoMapper;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Library;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands
{
    public class updateBooksCommandHandler : IHandlerWrapper<updateBooksCommand, bool>
    {
        private IApplicationDbContext context;
        private IMapper _mapper;

        public updateBooksCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }
        public async Task<Response<bool>> Handle(updateBooksCommand request, CancellationToken cancellationToken)
        {
            return await UpdateAuthorAsync(request.BooksDTO);
        }

        private async Task<Response<bool>> UpdateAuthorAsync(BooksDTO BooksDTO)
        {
            try
            {
                var bookOld = await context.Books.FindAsync(BooksDTO.ISBN);
                if (bookOld == null)
                    throw new BusinessException($"El Libro con ISBN :{BooksDTO.ISBN}, no exite.");

                var authors = await context.Authors.FindAsync(int.Parse(BooksDTO.AutorName));

                Domain.Entities.Library.Books author = bookOld;
                author.Title = BooksDTO.Title;
                author.Synopsis = BooksDTO.Synopsis;
                author.N_Pages = BooksDTO.N_Pages;

                context.Books.Update(author);
                await context.SaveChangesAsync();

                var autorBook = context.AuthorsHaveBooks.Where(x => x.BooksISBN == BooksDTO.ISBN && x.AuthorsId == authors.AuthorsId).FirstOrDefault();

                if (autorBook == null)
                {
                    autorBook = new Domain.Entities.People.AuthorsHaveBooks
                    {
                        AuthorsId = authors.AuthorsId,
                        BooksISBN = BooksDTO.ISBN
                    };

                    context.AuthorsHaveBooks.Add(autorBook);
                    await context.SaveChangesAsync();
                }

                return await Task.FromResult(Response.Ok(true));
            }
            catch (Exception ex)
            {
                throw new BusinessException(ex.Message);
            }
        }
    }
}
