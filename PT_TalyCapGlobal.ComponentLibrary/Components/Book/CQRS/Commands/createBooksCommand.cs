
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Library;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands
{
    public class createBooksCommand : IRequestWrapper<bool>
    {
        public BooksDTO BooksDTO { get; }
        public createBooksCommand(BooksDTO BooksDTO)
        {
            this.BooksDTO = BooksDTO;
        }
    }
}
