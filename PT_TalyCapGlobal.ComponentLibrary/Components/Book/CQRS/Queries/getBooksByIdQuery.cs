using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Library;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Queries
{
    public class getBooksByIdQuery : IRequestWrapper<BooksDTO>
    {
        public int ISBN { get; }
        public getBooksByIdQuery(int ISBN)
        {
            this.ISBN = ISBN;
        }
    }
}
