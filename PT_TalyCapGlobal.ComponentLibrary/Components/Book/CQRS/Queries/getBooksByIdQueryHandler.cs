using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Library;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Queries
{
    public class GetBooksByIdQueryHandler : IHandlerWrapper<getBooksByIdQuery, BooksDTO>
    {
        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public GetBooksByIdQueryHandler(IApplicationDbContext context, IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<BooksDTO>> Handle(getBooksByIdQuery request, CancellationToken cancellationToken)
        {
            return await GetBooksFromId(request.ISBN);
        }

        private async Task<Response<BooksDTO>> GetBooksFromId(int id)
        {
            var Book = await Context.Books.FirstOrDefaultAsync(b => b.ISBN == id);
            if (Book == null)
                throw new BusinessException($"El Libro con ISBN: {id}, no exite.");

            BooksDTO BooksDTO = new BooksDTO
            {
                ISBN = Book.ISBN,
                Title = Book.Title,
                Synopsis = Book.Synopsis,
                N_Pages = Book.N_Pages,
                PublishDate = Book.Created
            };
            return await Task.FromResult(Response.Ok(BooksDTO));
        }

    }
}
