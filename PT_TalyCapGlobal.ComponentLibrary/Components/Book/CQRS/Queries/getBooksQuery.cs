using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Library;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Collections.Generic;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Queries
{
    public class getBooksQuery : IRequestWrapper<List<BooksDTO>>
    {
        public getBooksQuery(FiltersBooks FiltersBooks)
        {
            this.FiltersBooks = FiltersBooks;
        }

        public FiltersBooks FiltersBooks { get; }
    }
}
