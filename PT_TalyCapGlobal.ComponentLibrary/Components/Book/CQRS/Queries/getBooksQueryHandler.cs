using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Library;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Queries
{
    public class GetBooksQueryHandler : IHandlerWrapper<getBooksQuery, List<BooksDTO>>
    {
        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public GetBooksQueryHandler(IApplicationDbContext context, IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<List<BooksDTO>>> Handle(getBooksQuery request, CancellationToken cancellationToken)
        {
            return await GetAllBooks(request.FiltersBooks);
        }

        private async Task<Response<List<BooksDTO>>> GetAllBooks(FiltersBooks filtersBooks)
        {
            var Books = await Context.Books.ToListAsync();
            List<BooksDTO> BooksDTO = new List<BooksDTO>();
            Books.ForEach(x =>
            {
                BooksDTO.Add(
                   new BooksDTO
                   {
                       ISBN = x.ISBN,
                       Title = x.Title,
                       Synopsis = x.Synopsis,
                       N_Pages = x.N_Pages,
                       AutorName = string.Join(", ", Context.AuthorsHaveBooks.Include(x => x.Authors).Where(c => c.BooksISBN == x.ISBN).Select(x => $"{x.Authors.Names} {x.Authors.Surnames}")),
                       PublishDate = x.Created
                   });
            });


            if (string.IsNullOrWhiteSpace(filtersBooks.AuthorName) && !filtersBooks.StartDate.HasValue && !filtersBooks.FinalhDate.HasValue)
                return await Task.FromResult(Response.Ok(BooksDTO));
            else if (!string.IsNullOrWhiteSpace(filtersBooks.AuthorName) && !filtersBooks.StartDate.HasValue && !filtersBooks.FinalhDate.HasValue)
                return await Task.FromResult(Response.Ok(BooksDTO.Where(b => b.AutorName.ToUpper().Contains(filtersBooks.AuthorName.ToUpper())).ToList()));
            else if (string.IsNullOrWhiteSpace(filtersBooks.AuthorName) && filtersBooks.StartDate.HasValue && !filtersBooks.FinalhDate.HasValue)
                return await Task.FromResult(Response.Ok(BooksDTO.Where(b => b.PublishDate >= filtersBooks.StartDate).ToList()));
            else if (string.IsNullOrWhiteSpace(filtersBooks.AuthorName) && filtersBooks.StartDate.HasValue && filtersBooks.FinalhDate.HasValue)
                return await Task.FromResult(Response.Ok(BooksDTO.Where(b => b.PublishDate >= filtersBooks.StartDate && b.PublishDate <= filtersBooks.FinalhDate).ToList()));
            else if (string.IsNullOrWhiteSpace(filtersBooks.AuthorName) && !filtersBooks.StartDate.HasValue && filtersBooks.FinalhDate.HasValue)
                return await Task.FromResult(Response.Ok(BooksDTO.Where(b => b.PublishDate <= filtersBooks.FinalhDate).ToList()));
            else
                return await Task.FromResult(Response.Ok(BooksDTO));
        }

    }
}
