using MediatR;
using Microsoft.Extensions.Logging;
using PT_TalyCapGlobal.Domain.Events;
using PT_TalyCapGlobal.Services.Events;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.EventHandlers
{
    public class AuthorsCompletedEventHandler : INotificationHandler<DomainEventNotification<EntitiesCreatedEvent<Domain.Entities.People.Authors>>>
    {
        private readonly ILogger<AuthorsCompletedEventHandler> _logger;

        public AuthorsCompletedEventHandler(ILogger<AuthorsCompletedEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(DomainEventNotification<EntitiesCreatedEvent<Domain.Entities.People.Authors>> notification, CancellationToken cancellationToken)
        {
            var domainEvent = notification.DomainEvent;

            _logger.LogInformation("ComponentLibrary Domain Event: {DomainEvent}", domainEvent.GetType().Name);

            return Task.CompletedTask;
        }
    }
}