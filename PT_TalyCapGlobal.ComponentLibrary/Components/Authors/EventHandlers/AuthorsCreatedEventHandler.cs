using MediatR;
using Microsoft.Extensions.Logging;
using PT_TalyCapGlobal.Domain.Events;
using PT_TalyCapGlobal.Services.Events;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.EventHandlers
{
    public class AuthorsCreatedEventHandler : INotificationHandler<DomainEventNotification<EntitiesCreatedEvent<Domain.Entities.People.Authors>>>
    {
        private readonly ILogger<AuthorsCreatedEventHandler> _logger;

        public AuthorsCreatedEventHandler(ILogger<AuthorsCreatedEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(DomainEventNotification<EntitiesCreatedEvent<Domain.Entities.People.Authors>> notification, CancellationToken cancellationToken)
        {
            var domainEvent = notification.DomainEvent;

            _logger.LogInformation("ComponentLibrary Domain Event: {DomainEvent}", domainEvent.GetType().Name);

            return Task.CompletedTask;
        }
    }
}