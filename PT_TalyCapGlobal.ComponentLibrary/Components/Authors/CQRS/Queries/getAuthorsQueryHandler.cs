using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Author;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Queries
{
    public class GetAuthorsQueryHandler : IHandlerWrapper<getAuthorsQuery, List<AuthorDTO>>
    {
        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public GetAuthorsQueryHandler(IApplicationDbContext context, IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<List<AuthorDTO>>> Handle(getAuthorsQuery request, CancellationToken cancellationToken)
        {
            return await GetAllAuthors();
        }

        private async Task<Response<List<AuthorDTO>>> GetAllAuthors()
        {
            var Authors = await Context.Authors.ToListAsync();
            List<AuthorDTO> AuthorDTO = new List<AuthorDTO>();
            Authors.ForEach(x =>
            {
                AuthorDTO.Add(
                   new AuthorDTO
                   {
                       AuthorsId = x.AuthorsId,
                       Names = x.Names,
                       Surnames = x.Surnames
                   });
            });

            return await Task.FromResult(Response.Ok(AuthorDTO));
        }

    }
}
