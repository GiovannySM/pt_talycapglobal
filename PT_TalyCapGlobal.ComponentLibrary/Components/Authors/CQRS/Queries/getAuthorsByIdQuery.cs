using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Author;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Queries
{
    public class getAuthorsByIdQuery : IRequestWrapper<AuthorDTO>
    {
        public int id { get; }
        public getAuthorsByIdQuery(int id)
        {
            this.id = id;
        }
    }
}
