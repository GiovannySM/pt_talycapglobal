using AutoMapper;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Author;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Queries
{
    public class GetAuthorsByIdQueryHandler : IHandlerWrapper<getAuthorsByIdQuery, AuthorDTO>
    {
        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public GetAuthorsByIdQueryHandler(IApplicationDbContext context, IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<AuthorDTO>> Handle(getAuthorsByIdQuery request, CancellationToken cancellationToken)
        {
            return await GetAuthorsFromId(request.id);
        }

        private async Task<Response<AuthorDTO>> GetAuthorsFromId(int id)
        {
            var Author = await Context.Authors.FindAsync(id);
            if (Author == null)
                throw new BusinessException($"El Autor con Id: {id}, no exite.");

            AuthorDTO AuthorDTO = new AuthorDTO
            {
                AuthorsId = Author.AuthorsId,
                Names = Author.Names,
                Surnames = Author.Surnames
            };
            return await Task.FromResult(Response.Ok(AuthorDTO));
        }

    }
}
