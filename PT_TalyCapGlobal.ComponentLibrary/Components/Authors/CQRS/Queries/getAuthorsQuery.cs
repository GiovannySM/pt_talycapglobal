using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Author;
using System.Collections.Generic;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Queries
{
    public class getAuthorsQuery : IRequestWrapper<List<AuthorDTO>>
    {
        public getAuthorsQuery()
        { }
    }
}
