using AutoMapper;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Author;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands
{
    public class updateAuthorsCommandHandler : IHandlerWrapper<updateAuthorsCommand, bool>
    {
        private IApplicationDbContext context;
        private IMapper _mapper;

        public updateAuthorsCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }
        public async Task<Response<bool>> Handle(updateAuthorsCommand request, CancellationToken cancellationToken)
        {
            return await UpdateAuthorAsync(request.authorDTO);
        }

        private async Task<Response<bool>> UpdateAuthorAsync(AuthorDTO authorDTO)
        {
            try
            {
                var authorOld = await context.Authors.FindAsync(authorDTO.AuthorsId);
                if (authorOld == null)
                    throw new BusinessException($"El Autor con Id :{authorDTO.AuthorsId}, no exite.");

                Domain.Entities.People.Authors author = authorOld;
                author.Names = authorDTO.Names;
                author.Surnames = authorDTO.Surnames;
                context.Authors.Update(author);
                await context.SaveChangesAsync();
                return await Task.FromResult(Response.Ok(true));
            }
            catch (Exception ex)
            {
                throw new BusinessException(ex.Message);
            }
        }
    }
}
