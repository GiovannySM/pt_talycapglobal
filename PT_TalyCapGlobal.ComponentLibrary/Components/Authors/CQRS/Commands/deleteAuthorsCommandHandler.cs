using AutoMapper;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands
{
    public class deleteAuthorsCommandHandler : IHandlerWrapper<deleteAuthorsCommand, bool>
    {
        private IApplicationDbContext context;
        private IMapper _mapper;

        public deleteAuthorsCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            _mapper = mapper;
        }
        public async Task<Response<bool>> Handle(deleteAuthorsCommand request, CancellationToken cancellationToken)
        {
            return await deleteAuthorAsync(request.authorId);
        }

        private async Task<Response<bool>> deleteAuthorAsync(int authorId)
        {
            try
            {
                var author = await context.Authors.FindAsync(authorId);
                if (author == null)
                    throw new BusinessException($"El Autor con Id :{authorId}, no exite.");
                context.Authors.Remove(author);
                await context.SaveChangesAsync();
                return await Task.FromResult(Response.Ok(true));
            }
            catch (Exception ex)
            {
                throw new BusinessException(ex.Message);
            }
        }
    }
}
