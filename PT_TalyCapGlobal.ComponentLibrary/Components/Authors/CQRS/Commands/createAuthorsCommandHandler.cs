using AutoMapper;
using PT_TalyCapGlobal.ComponentLibrary.Common.Mappings;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Author;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands
{
    public class createAuthorsCommandHandler : IHandlerWrapper<createAuthorsCommand, bool>
    {

        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public createAuthorsCommandHandler(IApplicationDbContext context, IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<bool>> Handle(createAuthorsCommand request, CancellationToken cancellationToken)
        {
            return await CreateAuthorAsync(request.AuthorDTO);
        }

        private async Task<Response<bool>> CreateAuthorAsync(AuthorDTO AuthorDTO)
        {
            try
            {
                var Author = MappingExtensions.Map<AuthorDTO, Domain.Entities.People.Authors>(AuthorDTO);
                Context.Authors.Add(Author);
                await Context.SaveChangesAsync();
                return await Task.FromResult(Response.Ok(true));
            }
            catch (Exception)
            {
                return await Task.FromResult(Response.Fail<bool>(false));
            }
        }
    }
}
