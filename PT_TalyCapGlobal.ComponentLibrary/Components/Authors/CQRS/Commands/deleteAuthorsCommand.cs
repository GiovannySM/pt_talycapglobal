
using PT_TalyCapGlobal.Services.IWrappersCQRS;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands
{
    public class deleteAuthorsCommand : IRequestWrapper<bool>
    {
        public int authorId { get; }
        public deleteAuthorsCommand(int authorId)
        {
            this.authorId = authorId;
        }
    }
}
