
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Author;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands
{
    public class createAuthorsCommand : IRequestWrapper<bool>
    {
        public AuthorDTO AuthorDTO { get; }
        public createAuthorsCommand(AuthorDTO AuthorDTO)
        {
            this.AuthorDTO = AuthorDTO;
        }
    }
}
