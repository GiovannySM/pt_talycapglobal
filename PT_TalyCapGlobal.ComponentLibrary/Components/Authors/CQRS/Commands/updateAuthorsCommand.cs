
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Author;

namespace PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands
{
    public class updateAuthorsCommand : IRequestWrapper<bool>
    {
        public AuthorDTO authorDTO { get; }
        public updateAuthorsCommand(AuthorDTO authorDTO)
        {
            this.authorDTO = authorDTO;
        }
    }
}
