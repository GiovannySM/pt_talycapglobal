using PT_TalyCapGlobal.Authentication.Common.Models;
using PT_TalyCapGlobal.Services.IWrappersCQRS;

namespace PT_TalyCapGlobal.Authentication.Components.Account.CQRS.Queries
{
    public class LoginQuery : IRequestWrapper<LoginResponse>
    {
        public Login Login { get; }

        public LoginQuery(Login Login)
        {
            this.Login = Login;
        }
    }
}
