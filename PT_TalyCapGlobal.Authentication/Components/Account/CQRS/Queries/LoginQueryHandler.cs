using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using PT_TalyCapGlobal.Authentication.Common.Models;
using PT_TalyCapGlobal.Common.Helpers;
using PT_TalyCapGlobal.Domain.Identity;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Authentication.Components.Account.CQRS.Queries
{
    public class LoginQueryHandler : IHandlerWrapper<LoginQuery, LoginResponse>
    {
        private UserManager<ApplicationUser> UserManager;
        private IApplicationDbContext Context;
        private ClaimsPrincipal ClaimsPrincipal;

        public LoginQueryHandler(
            UserManager<ApplicationUser> userManager,
            IApplicationDbContext context)
        {
            UserManager = userManager;
            Context = context;
        }
        public async Task<Response<LoginResponse>> Handle(LoginQuery request, CancellationToken cancellationToken)
        {

            var (ClaimPrincipal, Claims, IsLocked) = await SignInAsync(request.Login.UserName, request.Login.Password);
            string token;
            IsAuthenticated(Claims, out token);

            return await Task.FromResult(Response.Ok(new LoginResponse() { Token = token }, $"Authenticated {request.Login.UserName}"));
        }

        public async Task<(ClaimsPrincipal ClaimPrincipal, List<Claim> Claims, bool IsLocked)> SignInAsync(string userName, string password)
        {
            bool IsLocked = false;
            bool status = false;

            var user = await UserManager.FindByNameAsync(userName);

            if (user == null)
                throw new BusinessException($"El usuario, no existe.");

            if (user.AccessFailedCount >= 3)
                throw new BusinessException($"El usuario, esta bloqueado.");

            status = await UserManager.CheckPasswordAsync(user, password);

            var claims = new List<Claim>();
            IList<string> roleUser = new List<string>();

            if (status)
            {
                roleUser = await UserManager.GetRolesAsync(user);
                var role = roleUser.FirstOrDefault() == null ? "Standar" : roleUser.FirstOrDefault();

                claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(ClaimTypes.NameIdentifier, user.Id),
                        new Claim(ClaimTypes.Email, user.Email),
                        new Claim(ClaimTypes.WindowsAccountName, user.UserName),
                        new Claim(ClaimTypes.Role,role),
                    };

                ClaimsPrincipal = new ClaimsPrincipal(
                 new ClaimsIdentity(claims: claims, authenticationType: "Password", nameType: user.UserName, roleType: roleUser.FirstOrDefault())
                 );

                var isAuthenticated = ClaimsPrincipal.Identity.IsAuthenticated; // true

                IsLocked = false;
                user.LockoutEnabled = IsLocked;
                user.AccessFailedCount = 0;
                Context.ApplicationUser.Update(user);
            }
            else
            {
                user.AccessFailedCount = user.AccessFailedCount + 1;
                IsLocked = user.AccessFailedCount >= 3;
                user.LockoutEnd = DateTime.Now.AddDays(2);
                user.LockoutEnabled = IsLocked;

                Context.ApplicationUser.Update(user);
                await Context.SaveChangesAsync();

                throw new BusinessException($"El password, es invalido.");
            }


            return await Task.FromResult((ClaimsPrincipal, claims, IsLocked));
        }

        private bool IsAuthenticated(List<Claim> claims, out string token)
        {
            token = string.Empty;

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(HelperConfiguration.GetTokenManagement().Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var jwtToken = new JwtSecurityToken(
                HelperConfiguration.GetTokenManagement().Issuer,
                HelperConfiguration.GetTokenManagement().Audience,
                claims,
                expires: DateTime.Now.AddMinutes(HelperConfiguration.GetTokenManagement().AccessExpiration),
                signingCredentials: credentials
            );

            token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            return true;
        }

    }
}
