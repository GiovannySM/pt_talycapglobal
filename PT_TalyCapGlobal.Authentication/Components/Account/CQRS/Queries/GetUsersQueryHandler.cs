using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.Authentication.Common.Models;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Authentication.Components.Account.CQRS.Queries
{
    public class GetUsersQueryHandler : IHandlerWrapper<GetUsersQuery, List<Users>>
    {
        private IApplicationDbContext Context;
        private IMapper _Mapper;

        public GetUsersQueryHandler(
            IApplicationDbContext context,
            IMapper Mapper)
        {
            Context = context;
            _Mapper = Mapper;
        }
        public async Task<Response<List<Users>>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            return await GetUsers();
        }

        private async Task<Response<List<Users>>> GetUsers()
        {
            var Entity = await Context.ApplicationUser.ToListAsync();

            var EntityDto = Entity.Select(x => new Users
            {
                Id = x.Id,
                UserName = x.UserName,
                Email = x.Email,
                EmailConfirmed = x.EmailConfirmed,
                PhoneNumber = x.PhoneNumber,
                PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                LockoutEnd = x.LockoutEnd?.Date,
                LockoutEnabled = x.LockoutEnabled,
                TwoFactorEnabled = x.TwoFactorEnabled
            }).Where(x => x.UserName != "AdminMinosIT").ToList();

            return await Task.FromResult(Response.Ok(EntityDto));
        }
    }
}
