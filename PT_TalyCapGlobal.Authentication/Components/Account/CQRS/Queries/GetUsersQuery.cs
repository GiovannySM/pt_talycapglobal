using PT_TalyCapGlobal.Authentication.Common.Models;
using PT_TalyCapGlobal.Services.IWrappersCQRS;
using System.Collections.Generic;

namespace PT_TalyCapGlobal.Authentication.Components.Account.CQRS.Queries
{
    public class GetUsersQuery : IRequestWrapper<List<Users>>
    {
    }
}
