using FluentValidation;
using PT_TalyCapGlobal.Authentication.Common.Models;

namespace PT_TalyCapGlobal.Authentication.Components.Account.CQRS.Queries
{
    public class LoginQueryValidator : AbstractValidator<Login>
    {
        public LoginQueryValidator()
        {
            RuleFor(entity => entity.UserName)
                .NotEmpty()
                .WithMessage("'Usuario': Es obligatorio")
                .NotEqual("string")
                .MinimumLength(5)
                .WithName("Usuario");

            RuleFor(entity => entity.Password)
                .NotEmpty()
                .WithMessage("'Password': Es obligatorio")
                .NotEqual("string")
                .MinimumLength(8)
                .WithName("Password");
        }
    }

}



