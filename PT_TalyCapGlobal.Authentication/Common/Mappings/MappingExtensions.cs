using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace PT_TalyCapGlobal.Authentication.Common.Mappings
{
    public static class MappingExtensions
    {
        public static TDestination Map<TSource, TDestination>(this TSource source, IConfigurationProvider configuration)
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TSource, TDestination>().ReverseMap();
            });
            return config.CreateMapper().Map<TDestination>(source);
        }
        public static List<TDestination> Map<TSource, TDestination>(this IQueryable source, IConfigurationProvider configuration)
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TSource, TDestination>().ReverseMap();
            });
            return config.CreateMapper().Map<List<TDestination>>(source);
        }

        internal static List<TDestination> Map<TSource, TDestination>(this List<TSource> source, IConfigurationProvider configurationProvider)
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TSource, TDestination>().ReverseMap();
            });
            return config.CreateMapper().Map<List<TDestination>>(source.AsQueryable());
        }
    }
}
