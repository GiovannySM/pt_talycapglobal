namespace PT_TalyCapGlobal.Authentication.Common.Models
{
    public class LoginResponse
    {
        public string Token { get; set; }
    }
}
