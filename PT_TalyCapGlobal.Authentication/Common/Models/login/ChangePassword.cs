namespace PT_TalyCapGlobal.Authentication.Common.Models.login
{
    public class ChangePassword
    {
        public string UserName { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
