
namespace PT_TalyCapGlobal.Authentication.Common.Models
{
    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
