namespace PT_TalyCapGlobal.Authentication.Common.Models
{
    public class UsersCreate
    {
        public string IdentificationNumber { get; set; }
        public string Names { get; set; }
        public string Surnames { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string ProfileId { get; set; }
    }
}
