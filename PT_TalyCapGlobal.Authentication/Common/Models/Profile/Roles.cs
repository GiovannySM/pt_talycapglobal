namespace PT_TalyCapGlobal.Authentication.Common.Models
{
    public class Roles
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
