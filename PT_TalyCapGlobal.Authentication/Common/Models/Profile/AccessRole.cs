namespace PT_TalyCapGlobal.Authentication.Common.Models.Profile
{
    public class AccessRole
    {
        public int AccessRolesId { get; set; }
        public int ParentAccessRolesId { get; set; }
        public string ProfileName { get; set; }
        public string AccessName { get; set; }
        public string AccessIconClass { get; set; }
        public string AccessRouteName { get; set; }
        public int AccessOrder { get; set; }
    }
}
