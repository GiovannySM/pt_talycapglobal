using System;

namespace PT_TalyCapGlobal.Shared
{
    public class BusinessException : Exception
    {
        public BusinessException()
        { }

        public BusinessException(string message) : base(message)
        {

        }
    }
}
