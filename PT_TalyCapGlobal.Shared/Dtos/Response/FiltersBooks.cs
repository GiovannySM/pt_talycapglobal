﻿using System;

namespace PT_TalyCapGlobal.Shared.Dtos.Response
{
    public class FiltersBooks
    {
        public string AuthorName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinalhDate { get; set; }
    }
}
