namespace PT_TalyCapGlobal.Shared.Dtos.Response
{
    public class Errors
    {
        public string[] error { get; set; }
    }
}