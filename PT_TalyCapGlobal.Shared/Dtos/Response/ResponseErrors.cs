namespace PT_TalyCapGlobal.Shared.Dtos.Response
{
    public class ResponseErrors
    {
        public string[] errors { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public int status { get; set; }
        public string traceId { get; set; }
    }
}
