using System;

namespace PT_TalyCapGlobal.Shared.Dtos.Response
{
    public static class Response
    {
        public static Response<T> Fail<T>(string message, T data = default) => new Response<T>(data, message, false);
        public static Response<T> Fail<T>(bool error) => new Response<T>(error);
        public static Response<T> Fail<T>(bool error, Exception exception, T data = default) => new Response<T>(data, exception, false);
        public static Response<T> Fail<T>(string message, bool error) => new Response<T>(message, false);
        public static Response<T> Fail<T>(Exception ex) => new Response<T>(false, ex);

        public static Response<T> Ok<T>(T data, string message) => new Response<T>(data, message, true);
        public static Response<T> Ok<T>(string message) => new Response<T>(message, true);
        public static Response<T> Ok<T>(T data) => new Response<T>(data);

    }

    public class Response<T>
    {
        public T Data { get; }
        public string Message { get; }
        public bool Success { get; set; } = true;

        public Response(T data)
        {
            this.Data = data;
        }

        public Response(string msg, bool error)
        {
            this.Message = msg;
            this.Success = error;
        }

        public Response(bool error, string msg = "")
        {
            this.Message = msg;
            this.Success = error;
        }

        public Response(bool error, Exception exception)
        {
            this.Message = exception.InnerException == null ? exception.Message : exception.InnerException.Message;
            this.Success = error;
        }

        public Response(T data, string msg, bool error)
        {

            this.Data = data;
            this.Message = msg;
            this.Success = error;
        }

        public Response(T data, Exception exception, bool error)
        {
            this.Data = data;
            this.Message = exception.InnerException == null ? exception.Message : exception.InnerException.Message;
            this.Success = error;
        }


    }
}
