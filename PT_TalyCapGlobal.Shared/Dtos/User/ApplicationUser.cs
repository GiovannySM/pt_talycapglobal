﻿using Microsoft.AspNetCore.Identity;

namespace PT_TalyCapGlobal.Shared.Dtos.User
{
    public class ApplicationUser : IdentityUser
    {
    }
}
