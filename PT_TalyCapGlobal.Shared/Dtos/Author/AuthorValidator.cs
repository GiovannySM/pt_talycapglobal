
using FluentValidation;
using System.Globalization;

namespace PT_TalyCapGlobal.Shared.Dtos.Author
{
    public class AuthorValidator : AbstractValidator<AuthorDTO>
    {
        public AuthorValidator()
        {
            ValidatorOptions.LanguageManager.Culture = new CultureInfo("es-CO");

            RuleFor(entity => entity.Names)
                  .NotEmpty()
                  .NotNull()
                  .NotEqual("string")
                  .MinimumLength(2)
                  .MaximumLength(45)
                  .WithName("Nombres");

            RuleFor(entity => entity.Surnames)
                  .NotEmpty()
                  .NotNull()
                  .NotEqual("string")
                  .MinimumLength(2)
                  .MaximumLength(45)
                  .WithName("Apellidos");
        }
    }
}