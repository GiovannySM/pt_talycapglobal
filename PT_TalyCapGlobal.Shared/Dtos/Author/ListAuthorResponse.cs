using System.Collections.Generic;

namespace PT_TalyCapGlobal.Shared.Dtos.Author
{
    public class ListAuthorResponse
    {
        public List<AuthorDTO> data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
    }
}
