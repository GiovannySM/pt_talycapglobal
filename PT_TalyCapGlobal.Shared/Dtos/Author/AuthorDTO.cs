namespace PT_TalyCapGlobal.Shared.Dtos.Author
{
    public class AuthorDTO
    {
        public int AuthorsId { get; set; }
        public string Names { get; set; }
        public string Surnames { get; set; }
    }
}
