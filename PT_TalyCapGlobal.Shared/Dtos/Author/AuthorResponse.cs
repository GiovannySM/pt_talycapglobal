
namespace PT_TalyCapGlobal.Shared.Dtos.Author
{
    public class AuthorResponse
    {
        public AuthorDTO data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
    }
}
