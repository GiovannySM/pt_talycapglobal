
using System;

namespace PT_TalyCapGlobal.Shared.Dtos.Library
{
    public class BooksDTO
    {
        public int ISBN { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public string N_Pages { get; set; }
        public string AutorName { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
