namespace PT_TalyCapGlobal.Shared.Dtos.Library
{
    public class BooksResponse
    {
        public BooksDTO data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
    }
}
