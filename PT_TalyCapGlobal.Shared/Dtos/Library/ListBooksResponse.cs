
using System.Collections.Generic;

namespace PT_TalyCapGlobal.Shared.Dtos.Library
{
    public class ListBooksResponse
    {
        public List<BooksDTO> data { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
    }
}
