
using FluentValidation;
using System.Globalization;

namespace PT_TalyCapGlobal.Shared.Dtos.Library
{
    public class BookValidator : AbstractValidator<BooksDTO>
    {
        public BookValidator()
        {
            ValidatorOptions.LanguageManager.Culture = new CultureInfo("es-CO");

            RuleFor(entity => entity.Title)
                  .NotEmpty()
                  .NotNull()
                  .NotEqual("string")
                  .WithName("Titulo del libro");

            RuleFor(entity => entity.Synopsis)
                  .NotEmpty()
                  .NotNull()
                  .NotEqual("string")
                  .WithName("Sinopsis");

            RuleFor(entity => entity.N_Pages)
                  .NotEmpty()
                  .NotNull()
                  .NotEqual("string")
                  .MinimumLength(2)
                  .MaximumLength(45)
                  .WithName("Número de paginas");
        }
    }
}
