using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PT_TalyCapGlobal.DataAccess.Context;
using PT_TalyCapGlobal.DataAccess.SeedData;
using PT_TalyCapGlobal.Domain.Identity;
using System;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Server
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var Host = CreateHostBuilder(args).Build();
            var ScopoFactory = Host.Services.GetService<IServiceScopeFactory>();
            using (var Scope = ScopoFactory.CreateScope())
            {
                var Services = Scope.ServiceProvider;
                var logger = Scope.ServiceProvider.GetRequiredService<ILogger<Program>>();

                try
                {
                    var Context = Services.GetRequiredService<ApplicationDbContext>();
                    var userManager = Services.GetRequiredService<UserManager<ApplicationUser>>();

                    logger.LogInformation("Starting process of migrating or initializing the database");

                    await ApplicationDbContextSeed.SeedDataBaseAsync(Context);
                    await ApplicationDbContextSeed.SeedDefaultRolesAsync(Context);
                    await ApplicationDbContextSeed.SeedDefaultUserAsync(userManager);
                    await ApplicationDbContextSeed.SeedSampleDataAsync(Context);

                    logger.LogInformation("Salem configuration");


                    logger.LogInformation("Successful process of migrating or initializing the database");
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "An error occurred while migrating or seeding the database.");
                    throw;
                }
            }
            Host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
