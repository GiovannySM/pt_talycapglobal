using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using System;
using System.Linq;

namespace PT_TalyCapGlobal.Server.SwaggerOptions.Extensions
{
    public static class ActionDescriptorExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionDescriptor"></param>
        /// <returns></returns>
        public static ApiVersionModel GetApiVersion(this ActionDescriptor actionDescriptor)
        {
            return (ApiVersionModel)actionDescriptor?.Properties
                .FirstOrDefault(w => ((Type)w.Key).Equals(typeof(ApiVersionModel))).Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="docName"></param>
        /// <param name="apiDescription"></param>
        /// <returns></returns>
        public static bool GetPredicate(string docName, ApiDescription apiDescription)
        {
            var actionApiVersionModel = apiDescription.ActionDescriptor?.GetApiVersion();
            if (actionApiVersionModel == null)
            {
                return true;
            }
            if (actionApiVersionModel.DeclaredApiVersions.Any())
            {
                return actionApiVersionModel.DeclaredApiVersions.Any(version => $"v{version.ToString()}".Equals(docName));
            }
            return actionApiVersionModel.ImplementedApiVersions.Any(version => $"v{version.ToString()}".Equals(docName));
        }

    }
}
