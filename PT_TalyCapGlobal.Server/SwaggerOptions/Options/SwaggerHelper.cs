using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PT_TalyCapGlobal.Server.SwaggerOptions.Options
{
    public class SwaggerHelper
    {
        internal const string NameServices = "Web API REST Full Prueba Tecnica Full Stack Angular .Net";
        internal const string NameCompany = "TalyCapGlobal";
        internal const string UrlCompany = "https://talycapglobal.com/";

        public static Func<IEnumerable<ApiDescription>, ApiDescription> ConflictingActionsResolver()
        {
            return apiDescriptions =>
            {
                var descriptions = apiDescriptions as ApiDescription[] ?? apiDescriptions.ToArray();

                var result = descriptions.First();
                var parameters = descriptions.SelectMany(d => d.ParameterDescriptions)
                                             .ToList();
                result.ParameterDescriptions.Clear();

                foreach (var parameter in parameters)
                {
                    if (result.ParameterDescriptions.All(x => x.Name != parameter.Name))
                    {
                        result.ParameterDescriptions.Add(new ApiParameterDescription
                        {
                            Name = parameter.Name,
                            ParameterDescriptor = parameter.ParameterDescriptor,
                            Source = parameter.Source
                        });
                    }
                }
                return result;
            };
        }

        internal static string GetDescription(double version)
        {
            switch (version)
            {
                case 1.0:
                    return $"Servicio REST v{version}, que se usa como Backend de las aplicaciones de la compañía.";
                case 2.0:
                    return $"Servicio REST v{version}, que se usa como Backend de las aplicaciones de la compañía.";
                default:
                    return "";
            }
        }
    }
}
