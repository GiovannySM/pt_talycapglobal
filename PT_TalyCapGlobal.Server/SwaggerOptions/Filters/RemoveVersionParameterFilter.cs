using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;

namespace PT_TalyCapGlobal.Server.SwaggerOptions.Filters
{
    public class RemoveVersionParameterFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var parameterVersion = operation?.Parameters?.SingleOrDefault(
                param => param.Name == "version");
            operation?.Parameters?.Remove(parameterVersion);
        }
    }
}
