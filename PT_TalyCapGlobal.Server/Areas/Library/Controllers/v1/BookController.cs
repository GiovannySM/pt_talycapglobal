using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Commands;
using PT_TalyCapGlobal.ComponentLibrary.Book.CQRS.Queries;
using PT_TalyCapGlobal.Server.Controllers;
using PT_TalyCapGlobal.Shared.Dtos.Library;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Server.Areas.Library.Controllers.v1
{
    [Authorize]
    [Route("Library/Books/v{version:apiVersion}")]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [ApiController]
    public class BookController : ApiController
    {

        /// <summary>
        /// Accion para recuperar la lista de libros
        /// </summary>
        /// <param name="FiltersBooks"></param>
        /// <returns></returns>
        [HttpGet("getBookAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<List<BooksDTO>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> getBookAsync(string AuthorName, DateTime? StartDate, DateTime? FinalhDate)
        {
            var resutl = await Mediator.Send(
                new getBooksQuery(
                    new FiltersBooks
                    {
                        AuthorName = AuthorName,
                        StartDate = StartDate,
                        FinalhDate = FinalhDate
                    }));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para recuperar un Autor por el Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getBookByIdAsync/{id}"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<BooksDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> getBookByIdAsync(int id)
        {
            var resutl = await Mediator.Send(new getBooksByIdQuery(id));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para crear libros
        /// </summary>
        /// <param name="Book"></param>
        /// <returns></returns>
        [HttpPost("createBookAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<BooksDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> createBookAsync([FromBody] BooksDTO Book)
        {
            var resutl = await Mediator.Send(new createBooksCommand(Book));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para actualizar libros
        /// </summary>
        /// <param name="Book"></param>
        /// <returns></returns>
        [HttpPut("updateBookAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<BooksDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> updateBookAsync([FromBody] BooksDTO Book)
        {
            var resutl = await Mediator.Send(new updateBooksCommand(Book));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para eliminar libros
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("deleteBookAsync/{id}"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<BooksDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> deleteBookAsync(int id)
        {
            var resutl = await Mediator.Send(new deleteBooksCommand(id));
            return Ok(resutl);
        }


    }
}
