using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Commands;
using PT_TalyCapGlobal.ComponentLibrary.Authors.CQRS.Queries;
using PT_TalyCapGlobal.Server.Controllers;
using PT_TalyCapGlobal.Shared.Dtos.Author;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Server.Areas.Library.Controllers.v1
{
    [Authorize]
    [Route("Library/Authors/v{version:apiVersion}")]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [ApiController]
    public class AuthorController : ApiController
    {

        /// <summary>
        /// Accion para recuperar la lista de Autores
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAuthorAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<List<AuthorDTO>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> getAuthorAsync()
        {
            var resutl = await Mediator.Send(new getAuthorsQuery());
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para recuperar un Autor por el Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getAuthorByIdAsync/{id}"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<AuthorDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> getAuthorByIdAsync(int id)
        {
            var resutl = await Mediator.Send(new getAuthorsByIdQuery(id));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para crear Autores
        /// </summary>
        /// <param name="author"></param>
        /// <returns></returns>
        [HttpPost("createAuthorAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<AuthorDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> createAuthorAsync([FromBody] AuthorDTO author)
        {
            var resutl = await Mediator.Send(new createAuthorsCommand(author));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para actualizar autores
        /// </summary>
        /// <param name="author"></param>
        /// <returns></returns>
        [HttpPut("updateAuthorAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<AuthorDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> updateAuthorAsync([FromBody] AuthorDTO author)
        {
            var resutl = await Mediator.Send(new updateAuthorsCommand(author));
            return Ok(resutl);
        }

        /// <summary>
        /// Accion para eliminar autores
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("deleteAuthorAsync/{id}"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<AuthorDTO>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> deleteAuthorAsync(int id)
        {
            var resutl = await Mediator.Send(new deleteAuthorsCommand(id));
            return Ok(resutl);
        }
    }
}
