﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PT_TalyCapGlobal.Authentication.Common.Models;
using PT_TalyCapGlobal.Authentication.Components.Account.CQRS.Queries;
using PT_TalyCapGlobal.Server.Controllers;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Net;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Server.Areas.Auth.Controllers.v1
{
    [Route("Account/v{version:apiVersion}")]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [ApiController]
    [Authorize(JwtBearerDefaults.AuthenticationScheme)]
    public class AccountController : ApiController
    {

        /// <summary>
        /// Login por medio de usuarios de application, usuarios almacenados en https://fakerestapi.azurewebsites.net/index.html
        /// </summary>
        /// <param name="login">Login</param>
        /// <remarks>
        /// Ejemplo Request:
        ///
        ///     {
        ///        "UserName": "Usuario aplicación",
        ///        "Password": "Password"
        ///     }
        ///
        /// </remarks>
        /// <response code="201">Si se autentico de forma correcta</response>
        /// <response code="400">Si el request body esta nulo, vacío o con errores de validación</response>    
        [AllowAnonymous]
        [HttpPost("LoginAsync"), MapToApiVersion("1.0")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Response<LoginResponse>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> LoginAsync([FromBody] Login login)
        {
            var resutl = await Mediator.Send(new LoginQuery(login));
            return Ok(resutl);
        }
    }
}
