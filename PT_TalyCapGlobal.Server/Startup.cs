using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PT_TalyCapGlobal.Authentication.DependencyInjection;
using PT_TalyCapGlobal.Common.Helpers;
using PT_TalyCapGlobal.ComponentLibrary.DependencyInjection;
using PT_TalyCapGlobal.DataAccess.Context;
using PT_TalyCapGlobal.Infrastructure.Dependency;
using PT_TalyCapGlobal.Server.Policies;
using PT_TalyCapGlobal.Server.SwaggerOptions.Extensions;
using PT_TalyCapGlobal.Server.SwaggerOptions.Filters;
using PT_TalyCapGlobal.Server.SwaggerOptions.Options;
using PT_TalyCapGlobal.Services.ApiValidators.Filters;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PT_TalyCapGlobal.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy("default", builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });

            // Injectamos la Infraestructura y Persistencia
            services.InjectInfrastructure(Configuration);

            // Injectamos el componente de Seguridad
            services.InjectComponentSecurity();

            // Injectamos el componente de Autores, Libros
            services.InjectComponentLibrary();

            services.AddHttpContextAccessor();

            // Customise default API behaviour
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //Registamos el API Versi?n
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", new OpenApiInfo
                {
                    Version = "v1.0",
                    Title = SwaggerHelper.NameServices,
                    Description = SwaggerHelper.GetDescription(1.0),
                    Contact = new OpenApiContact
                    {
                        Name = SwaggerHelper.NameCompany,
                        Email = string.Empty,
                        Url = new Uri(SwaggerHelper.UrlCompany),
                    }
                });

                //Resuele los conflictos de Swagger
                c.DocInclusionPredicate((docName, apiDesc) => ActionDescriptorExtension.GetPredicate(docName, apiDesc));
                c.ResolveConflictingActions(o => o.First());
                c.OperationFilter<RemoveVersionParameterFilter>();
                c.DocumentFilter<ReplaceVersoinWithExactValueInPath>();

                //Add SecurityDefinition
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "Authorization: Bearer token\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                //Add SecurityRequirement
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer" }
                                }, new List<string>()
                    }
                });

                // Add Comments
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)) + ".xml";
                if (File.Exists(xmlPath))
                {
                    c.IncludeXmlComments(xmlPath);
                }
                c.CustomSchemaIds(x => x.FullName);

                // Add Validation Rules With FluentValidationRules
                c.AddFluentValidationRules();
            });

            //Add Authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(HelperConfiguration.GetTokenManagement().Secret));
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // Llave para validar el Token
                        IssuerSigningKey = key,
                        ValidateIssuer = true,
                        ValidIssuer = HelperConfiguration.GetTokenManagement().Issuer,
                        ValidateAudience = true,
                        ValidAudience = HelperConfiguration.GetTokenManagement().Audience,
                        ValidateLifetime = true
                    };
                });

            //Add Authorization
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Bearer", policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.RequireAuthenticatedUser();
                });
            });

            // Adding MediatR for Domain Events and Notifications
            services.AddMediatR(typeof(Startup));

            services.AddControllersWithViews(options =>
            {
                //Se agrega el filto de excepciones globales
                options.Filters.Add<GlobalExceptionFilter>();
            }).AddNewtonsoftJson(options =>
            {
                //Ignora la validacion de referencias circulares
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                //Ignora las propiedades y entidades nulas
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            }).ConfigureApiBehaviorOptions(option =>
            {
                //Suprime la validacion implicita del modelo
                //option.SuppressModelStateInvalidFilter = true;
            }).AddFluentValidation();

            services.AddRazorPages();

            //Inject Policies
            services.AddAuthorization(options =>
            {
                options.AddPolicy(RolePolicies.Admin,
                                  RolePolicies.AdminPolicy);
                options.AddPolicy(RolePolicies.Accountant,
                                  RolePolicies.AccountantPolicy);
                options.AddPolicy(RolePolicies.Seller,
                                  RolePolicies.SellerPolicy);
                options.AddPolicy(RolePolicies.CompanyAgent,
                                  RolePolicies.CompanyPolicy);
                options.AddPolicy(RolePolicies.AllowAnonymous,
                                  RolePolicies.AnonymousPolicy);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var builder = HelperConfiguration.CreateDefaultBuilder(env.EnvironmentName);
            this.Configuration = builder.Build();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCors("default");
            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                if (env.IsDevelopment())
                {
                    c.DefaultModelsExpandDepth(-1);
                    c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "API v1.0");
                }
                else if (env.IsProduction())
                {
                    c.SwaggerEndpoint("v1.0/swagger.json", "API v1.0");
                }
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireCors("default");
            });
        }
    }
}
