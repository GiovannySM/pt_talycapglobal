using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using PT_TalyCapGlobal.Common.Helpers;
using PT_TalyCapGlobal.DataAccess.Context;
using PT_TalyCapGlobal.Services.IEvents;
using PT_TalyCapGlobal.Services.IServices;

namespace PT_TalyCapGlobal.Server.Services
{
    /// <summary>
    /// Establece el contexto en tiempo de ejecución para aplicar las migraciones de First-Code
    /// </summary>
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var ConnectionString = HelperConfiguration.GetConnectionConfiguration("DefaultConnection");
            builder.UseSqlServer(ConnectionString, x => x
                                .MigrationsHistoryTable("TalyCapGlobalHistoryEFCore", "Param")
                                .MigrationsAssembly("PT_TalyCapGlobal.DataAccessMigrations")
                                );
            return new ApplicationDbContext(builder.Options, new OperationalStoreOptionsMigrations());
        }
        public ApplicationDbContext CreateDbContextDependency(ICurrentUserService currentUserService, IDomainEventService domainEventService, IDateTimeService dateTime)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var ConnectionString = HelperConfiguration.GetConnectionConfiguration("DefaultConnection");
            builder.UseSqlServer(ConnectionString, x => x
                                .MigrationsHistoryTable("TalyCapGlobalHistoryEFCore", "Param")
                                .MigrationsAssembly("PT_TalyCapGlobal.DataAccessMigrations")
                                );
            return new ApplicationDbContext(builder.Options, new OperationalStoreOptionsMigrations(), currentUserService, domainEventService, dateTime);
        }
    }
}