﻿using Microsoft.AspNetCore.Authorization;

namespace PT_TalyCapGlobal.Server.Policies
{
    public class RolePolicies
    {
        public const string Admin = "Admin";
        public const string Accountant = "Accountant";
        public const string Seller = "Seller";
        public const string CompanyAgent = "CompanyAgent";
        public const string CompanyAdministrator = "CompanyAdministrator";
        public const string CompanyReporter = "CompanyReporter";
        public const string AllowAnonymous = "AllowAnonymous";

        public static AuthorizationPolicy AdminPolicy => new AuthorizationPolicyBuilder()
            .RequireAuthenticatedUser()
            .RequireRole(Admin)
            .Build();

        public static AuthorizationPolicy AccountantPolicy => new AuthorizationPolicyBuilder()
            .RequireAuthenticatedUser()
            .RequireRole(Accountant, Admin)
            .Build();

        public static AuthorizationPolicy SellerPolicy => new AuthorizationPolicyBuilder()
            .RequireAuthenticatedUser()
            .RequireRole(Seller, Admin)
            .Build();

        public static AuthorizationPolicy CompanyPolicy => new AuthorizationPolicyBuilder()
            .RequireAuthenticatedUser()
            .RequireRole(CompanyAdministrator, CompanyAgent, CompanyReporter)
            .Build();

        public static AuthorizationPolicy AnonymousPolicy => new AuthorizationPolicyBuilder()
            .RequireRole(AllowAnonymous)
            .Build();
    }
}
