using MediatR;
using PT_TalyCapGlobal.Domain.Common;

namespace PT_TalyCapGlobal.Services.Events
{
    public class DomainEventNotification<TDomainEvent> : INotification where TDomainEvent : DomainEvent
    {
        public DomainEventNotification(TDomainEvent domainEvent)
        {
            DomainEvent = domainEvent;
        }
        public TDomainEvent DomainEvent { get; }
    }
}
