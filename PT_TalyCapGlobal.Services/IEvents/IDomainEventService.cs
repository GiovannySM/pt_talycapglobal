using PT_TalyCapGlobal.Domain.Common;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Services.IEvents
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
