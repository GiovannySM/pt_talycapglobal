using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PT_TalyCapGlobal.Shared;
using System.Net;

namespace PT_TalyCapGlobal.Services.ApiValidators.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(BusinessException))
            {
                var exception = (BusinessException)context.Exception;

                var errors = new
                {
                    Message = exception.Message,
                };

                var json = new
                {
                    errors = new[] {
                        errors
                    },
                    Type = "BusinessException",
                    Title = "TitleBusinessException",
                    Status = HttpStatusCode.BadRequest,
                    TraceId = context.ActionDescriptor.Id
                };

                context.Result = new BadRequestObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.ExceptionHandled = true;
            }
            else
            {
                var validation = new
                {
                    Type = context.Exception.GetType().Name,
                    Status = HttpStatusCode.InternalServerError,
                    Title = context.Exception.InnerException == null ? context.Exception.Message : context.Exception.InnerException.Message
                };

                var errors = new
                {
                    Message = context.Exception.InnerException == null ? context.Exception.Message : context.Exception.InnerException.Message
                };

                var json = new
                {
                    errors = new[] {
                        errors
                    },
                    Type = "CustomException",
                    Status = HttpStatusCode.InternalServerError,
                    Title = "TitleCustomException",
                    TraceId = context.ActionDescriptor.Id
                };

                context.Result = new BadRequestObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.ExceptionHandled = true;
            }
        }
    }
}
