using PT_TalyCapGlobal.Services.IServices;
using System;

namespace PT_TalyCapGlobal.Services.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now => DateTime.Now;
    }
}
