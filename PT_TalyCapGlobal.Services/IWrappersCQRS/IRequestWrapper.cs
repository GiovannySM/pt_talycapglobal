using MediatR;
using PT_TalyCapGlobal.Shared.Dtos.Response;

namespace PT_TalyCapGlobal.Services.IWrappersCQRS
{
    public interface IRequestWrapper<T> : IRequest<Response<T>>
    { }
}
