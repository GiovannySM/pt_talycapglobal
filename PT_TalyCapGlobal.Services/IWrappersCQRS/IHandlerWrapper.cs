
using MediatR;
using PT_TalyCapGlobal.Shared.Dtos.Response;

namespace PT_TalyCapGlobal.Services.IWrappersCQRS
{
    public interface IHandlerWrapper<TIn, TOut> : IRequestHandler<TIn, Response<TOut>> where TIn : IRequestWrapper<TOut> { }
}
