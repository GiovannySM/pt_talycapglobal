using Microsoft.AspNetCore.Identity;
using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Linq;

namespace PT_TalyCapGlobal.Services.Identity
{
    public static class IdentityResultExtensions
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }
    }
}