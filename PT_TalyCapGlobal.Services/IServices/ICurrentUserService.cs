namespace PT_TalyCapGlobal.Services.IServices
{
    public interface ICurrentUserService
    {
        string UserId { get; }
    }
}
