using System;

namespace PT_TalyCapGlobal.Services.IServices
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}