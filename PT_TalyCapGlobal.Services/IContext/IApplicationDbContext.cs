using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.Domain.Entities.Library;
using PT_TalyCapGlobal.Domain.Entities.People;
using PT_TalyCapGlobal.Domain.Identity;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Services.IContext
{
    public interface IApplicationDbContext
    {
        DbSet<ApplicationRole> ApplicationRole { get; set; }
        DbSet<ApplicationUser> ApplicationUser { get; set; }
        DbSet<Authors> Authors { get; set; }
        DbSet<AuthorsHaveBooks> AuthorsHaveBooks { get; set; }
        DbSet<Books> Books { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}