using PT_TalyCapGlobal.Shared.Dtos.Response;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.Services.IIdentity
{
    public interface IIdentityService
    {
        Task<string> GetUserNameAsync(string userId);
        Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password);
        Task<Result> DeleteUserAsync(string userId);
        Task<string> GetUserIdByNameAsync(string UserName);
    }
}