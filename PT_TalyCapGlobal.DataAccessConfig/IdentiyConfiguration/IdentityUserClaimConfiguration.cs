using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PT_TalyCapGlobal.DataAccessConfig
{
    public class IdentityUserClaimConfiguration : IEntityTypeConfiguration<IdentityUserClaim<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserClaim<string>> builder)
        {
            builder
               .ToTable("UserClaims", "Users")
               .Property(p => p.Id)
               .HasColumnName("ClaimId");
        }
    }
}