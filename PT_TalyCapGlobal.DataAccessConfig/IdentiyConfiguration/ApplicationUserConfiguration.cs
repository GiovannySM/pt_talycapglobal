using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PT_TalyCapGlobal.Domain.Identity;

namespace PT_TalyCapGlobal.DataAccessConfig
{
    public class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.ToTable("Users", "Users")
            .Property(p => p.Id)
            .HasColumnName("UserId");
        }
    }
}
