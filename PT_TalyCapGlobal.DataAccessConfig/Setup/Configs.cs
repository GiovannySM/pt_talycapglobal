using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.DataAccessConfig.LibraryConfiguration;
using PT_TalyCapGlobal.DataAccessConfig.PeopleConfiguration;
using PT_TalyCapGlobal.Domain.Entities.Library;
using PT_TalyCapGlobal.Domain.Entities.People;
using PT_TalyCapGlobal.Domain.Identity;

namespace PT_TalyCapGlobal.DataAccessConfig
{
    public static class Configs
    {
        //Identity
        public static IEntityTypeConfiguration<ApplicationUser> ApplicationUserConfiguration => new ApplicationUserConfiguration();
        public static IEntityTypeConfiguration<IdentityRole> ApplicationRoleConfiguration => new ApplicationRoleConfiguration();
        public static IEntityTypeConfiguration<IdentityRoleClaim<string>> IdentityRoleClaimConfiguration => new IdentityRoleClaimConfiguration();
        public static IEntityTypeConfiguration<IdentityUserClaim<string>> IdentityUserClaimConfiguration => new IdentityUserClaimConfiguration();
        public static IEntityTypeConfiguration<IdentityUserLogin<string>> IdentityUserLoginConfiguration => new IdentityUserLoginConfiguration();
        public static IEntityTypeConfiguration<IdentityUserRole<string>> IdentityUserRoleConfiguration => new IdentityUserRoleConfiguration();
        public static IEntityTypeConfiguration<IdentityUserToken<string>> IdentityUserTokenConfiguration => new IdentityUserTokenConfiguration();

        //People
        public static IEntityTypeConfiguration<Authors> AuthorsConfiguration => new AuthorsConfiguration();
        public static IEntityTypeConfiguration<AuthorsHaveBooks> AuthorsHaveBooksConfiguration => new AuthorsHaveBooksConfiguration();

        //Products
        public static IEntityTypeConfiguration<Books> BooksConfiguration => new BooksConfiguration();

    }
}
