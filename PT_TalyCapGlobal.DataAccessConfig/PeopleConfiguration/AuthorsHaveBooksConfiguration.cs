using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PT_TalyCapGlobal.Domain.Entities.People;

namespace PT_TalyCapGlobal.DataAccessConfig.PeopleConfiguration
{
    public class AuthorsHaveBooksConfiguration : IEntityTypeConfiguration<AuthorsHaveBooks>
    {
        public void Configure(EntityTypeBuilder<AuthorsHaveBooks> builder)
        {
            builder.ToTable("AuthorsHaveBooks", "People")
                .HasKey(p => new { p.AuthorsId, p.BooksISBN });

            builder.HasOne(p => p.Authors)
                .WithMany()
                .HasForeignKey(p => p.AuthorsId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Books)
                .WithMany()
                .HasForeignKey(p => p.BooksISBN)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Ignore(e => e.DomainEvents);
        }
    }
}
