using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PT_TalyCapGlobal.Domain.Entities.People;

namespace PT_TalyCapGlobal.DataAccessConfig.PeopleConfiguration
{
    public class AuthorsConfiguration : IEntityTypeConfiguration<Authors>
    {
        public void Configure(EntityTypeBuilder<Authors> builder)
        {
            builder.ToTable("Authors", "People")
                .HasKey(p => p.AuthorsId);

            builder.Property(e => e.Names)
                .IsRequired()
                .HasMaxLength(45);

            builder.Property(e => e.Surnames)
                .IsRequired()
                .HasMaxLength(45);

            builder.Ignore(e => e.DomainEvents);
        }
    }
}
