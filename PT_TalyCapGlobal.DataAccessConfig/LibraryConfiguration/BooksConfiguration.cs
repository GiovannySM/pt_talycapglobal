using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PT_TalyCapGlobal.Domain.Entities.Library;

namespace PT_TalyCapGlobal.DataAccessConfig.LibraryConfiguration
{
    public class BooksConfiguration : IEntityTypeConfiguration<Books>
    {
        public void Configure(EntityTypeBuilder<Books> builder)
        {
            builder.ToTable("Books", "Library")
                .HasKey(p => p.ISBN);

            builder.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(45);

            builder.Property(e => e.Synopsis)
                .IsRequired()
                .HasColumnType("text");

            builder.Property(e => e.N_Pages)
                .IsRequired()
                .HasMaxLength(45);

            builder.Ignore(e => e.DomainEvents);

        }
    }
}