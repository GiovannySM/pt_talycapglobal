using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Options;
using PT_TalyCapGlobal.Common.Helpers;
using PT_TalyCapGlobal.Domain.Common;
using PT_TalyCapGlobal.Domain.Entities.Library;
using PT_TalyCapGlobal.Domain.Entities.People;
using PT_TalyCapGlobal.Domain.Identity;
using PT_TalyCapGlobal.Services.IContext;
using PT_TalyCapGlobal.Services.IEvents;
using PT_TalyCapGlobal.Services.IServices;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.DataAccess.Context
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTimeService _dateTime;
        private readonly IDomainEventService _domainEventService;
        private readonly DbContextOptionsBuilder<ApplicationDbContext> optionsBuilder;
        private readonly ConfigurationStoreOptions configStoreOptions;


        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions,
           ICurrentUserService currentUserService,
           IDomainEventService domainEventService,
           IDateTimeService dateTime) : base(options, operationalStoreOptions)
        {
            _currentUserService = currentUserService;
            _domainEventService = domainEventService;
            _dateTime = dateTime;
        }

        public ApplicationDbContext(DbContextOptions options,
                          IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataAccessConfig.Configs).Assembly);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var ConnectionString = HelperConfiguration.GetConnectionConfiguration("DefaultConnection");
            optionsBuilder.UseSqlServer(ConnectionString, x => x
                                        .MigrationsHistoryTable("TalyCapGlobalHistoryEFCore", "Param")
                                        .MigrationsAssembly("PT_TalyCapGlobal.DataAccessMigrations")
                                        );

        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (EntityEntry<AuditableEntity> entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            int result = await base.SaveChangesAsync(cancellationToken);

            await DispatchEvents(cancellationToken);

            return result;
        }

        public override int SaveChanges()
        {
            foreach (EntityEntry<AuditableEntity> entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            int result = base.SaveChanges();
            CancellationToken cancellationToken = new CancellationToken();
            DispatchEvents(cancellationToken).Wait();
            return result;
        }

        private async Task DispatchEvents(CancellationToken cancellationToken)
        {
            var domainEventEntities = ChangeTracker.Entries<IHasDomainEvent>()
                .Select(x => x.Entity.DomainEvents)
                .SelectMany(x => x)
                .ToArray();

            foreach (var domainEvent in domainEventEntities)
            {
                await _domainEventService.Publish(domainEvent);
            }
        }


        // Identity
        public DbSet<ApplicationUser> ApplicationUser { get; set; }
        public DbSet<ApplicationRole> ApplicationRole { get; set; }

        // Paoples
        public DbSet<Authors> Authors { get; set; }
        public DbSet<AuthorsHaveBooks> AuthorsHaveBooks { get; set; }

        // Library
        public DbSet<Books> Books { get; set; }
    }
}
