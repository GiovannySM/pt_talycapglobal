using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PT_TalyCapGlobal.DataAccess.Context;
using PT_TalyCapGlobal.Domain.Entities.Library;
using PT_TalyCapGlobal.Domain.Entities.People;
using PT_TalyCapGlobal.Domain.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PT_TalyCapGlobal.DataAccess.SeedData
{
    public static class ApplicationDbContextSeed
    {
        public static async Task<bool> SeedDataBaseAsync(ApplicationDbContext context)
        {
            try
            {
                if (context.Database.IsSqlServer())
                    context.Database.Migrate();
                return await Task.FromResult(true);
            }
            catch (Exception)
            {
                return await Task.FromResult(false);
            }

        }
        public static async Task<bool> SeedDefaultRolesAsync(ApplicationDbContext context)
        {
            try
            {
                if (!context.ApplicationRole.Any())
                {
                    context.ApplicationRole.Add(new ApplicationRole
                    {
                        Name = "Administrator",
                        NormalizedName = "Administrator"
                    });
                    await context.SaveChangesAsync();
                }
                return await Task.FromResult(true);
            }
            catch (Exception)
            {

                return await Task.FromResult(true);
            }

        }
        public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager)
        {
            try
            {
                string email = "pruebastecnicas@localhost.com.co";

                var userAdmin = new ApplicationUser
                {
                    UserName = email,
                    PhoneNumber = "3102361442",
                    Email = email,
                    EmailConfirmed = true,
                };

                if (userManager.Users.All(u => u.Email != userAdmin.Email))
                {
                    string Password = "Pruebastecnicas2020+-*";
                    var result = await userManager.CreateAsync(userAdmin, Password);
                    if (result.Succeeded)
                    {
                        _ = await userManager.AddToRoleAsync(userAdmin, "Administrator");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static async Task SeedSampleDataAsync(ApplicationDbContext context)
        {

            //Se agrega libros de prueba
            if (!(await context.Authors.AnyAsync()))
            {

                List<Authors> Authors = new List<Authors>();
                {
                    Authors.Add(new Authors { Names = "Jensen", Surnames = "Eric" });
                    Authors.Add(new Authors { Names = "Rolf", Surnames = "Lüders" });
                    Authors.Add(new Authors { Names = "José", Surnames = "Díaz" });
                }
                await context.Authors.AddRangeAsync(Authors);
                await context.SaveChangesAsync();
            }

            //Se agrega libros de prueba
            if (!(await context.Books.AnyAsync()))
            {
                List<Books> Entities = new List<Books>();
                {
                    Entities.Add(new Books { Title = "CHILE 1810-2010. La República en cifras I", Synopsis = "Prueba", N_Pages = "100" });
                    Entities.Add(new Books { Title = "CHILE 1810-2010. La República en cifras II", Synopsis = "Prueba", N_Pages = "100" });
                    Entities.Add(new Books { Title = "Constructores Inmigrantes. Transferencia", Synopsis = "Prueba", N_Pages = "25" });
                    Entities.Add(new Books { Title = "Cerebro y Aprendizaje. Competencias I", Synopsis = "Prueba", N_Pages = "60" });
                    Entities.Add(new Books { Title = "Cerebro y Aprendizaje. Competencias II", Synopsis = "Prueba", N_Pages = "80" });
                    Entities.Add(new Books { Title = "Cerebro y Aprendizaje. Competencias III", Synopsis = "Prueba", N_Pages = "80" });
                }
                await context.Books.AddRangeAsync(Entities);
                await context.SaveChangesAsync();
            }

            //Se agrega la relacion libros autores de prueba
            if (!(await context.AuthorsHaveBooks.AnyAsync()))
            {
                List<AuthorsHaveBooks> Entities = new List<AuthorsHaveBooks>();
                {
                    Entities.Add(new AuthorsHaveBooks { AuthorsId = (await context.Authors.FirstOrDefaultAsync(x => x.Names == "Jensen" && x.Surnames == "Eric")).AuthorsId, BooksISBN = (await context.Books.FirstOrDefaultAsync(x => x.Title == "CHILE 1810-2010. La República en cifras I")).ISBN });
                    Entities.Add(new AuthorsHaveBooks { AuthorsId = (await context.Authors.FirstOrDefaultAsync(x => x.Names == "Jensen" && x.Surnames == "Eric")).AuthorsId, BooksISBN = (await context.Books.FirstOrDefaultAsync(x => x.Title == "CHILE 1810-2010. La República en cifras II")).ISBN });
                    Entities.Add(new AuthorsHaveBooks { AuthorsId = (await context.Authors.FirstOrDefaultAsync(x => x.Names == "Rolf" && x.Surnames == "Lüders")).AuthorsId, BooksISBN = (await context.Books.FirstOrDefaultAsync(x => x.Title == "Constructores Inmigrantes. Transferencia")).ISBN });
                    Entities.Add(new AuthorsHaveBooks { AuthorsId = (await context.Authors.FirstOrDefaultAsync(x => x.Names == "José" && x.Surnames == "Díaz")).AuthorsId, BooksISBN = (await context.Books.FirstOrDefaultAsync(x => x.Title == "Cerebro y Aprendizaje. Competencias I")).ISBN });
                    Entities.Add(new AuthorsHaveBooks { AuthorsId = (await context.Authors.FirstOrDefaultAsync(x => x.Names == "José" && x.Surnames == "Díaz")).AuthorsId, BooksISBN = (await context.Books.FirstOrDefaultAsync(x => x.Title == "Cerebro y Aprendizaje. Competencias II")).ISBN });
                    Entities.Add(new AuthorsHaveBooks { AuthorsId = (await context.Authors.FirstOrDefaultAsync(x => x.Names == "José" && x.Surnames == "Díaz")).AuthorsId, BooksISBN = (await context.Books.FirstOrDefaultAsync(x => x.Title == "Cerebro y Aprendizaje. Competencias III")).ISBN });
                }
                await context.AuthorsHaveBooks.AddRangeAsync(Entities);
                await context.SaveChangesAsync();
            }
        }

    }
}


