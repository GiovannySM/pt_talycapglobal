
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PT_TalyCapGlobal.Common.Token;
using System;
using System.IO;

namespace PT_TalyCapGlobal.Common.Helpers
{
    public static class HelperConfiguration
    {
        public static string GetConnectionConfiguration(string connectionName, string configurationFile = "appsettings.json")
        {
            return new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                       .AddJsonFile(configurationFile, false)
                                       .Build()
                                       .GetConnectionString(connectionName);
        }

        public static void SetCongiguration(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = GetConnectionConfiguration("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString, x => x
                                        .MigrationsHistoryTable("TalyCapGlobalHistoryEFCore", "Param")
                                        .MigrationsAssembly("PT_TalyCapGlobal.DataAccessMigrations")
                                        );

        }

        public static ConfigurationBuilder CreateDefaultBuilder(string environmentName, string configurationFile = "appsettings")
        {
            return (ConfigurationBuilder)new ConfigurationBuilder()
                              .SetBasePath(Directory.GetCurrentDirectory())
                              .AddJsonFile($"{configurationFile}.{environmentName}.json", optional: false);

        }

        public static TokenManagement GetTokenManagement(string ConfigurationFile = "appsettings.json")
        {
            IConfiguration Configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationFile), optional: false)
                .Build();
            var Result = Configuration.GetSection("TokenManagement").Get<TokenManagement>();
            return Result;
        }
    }
}
